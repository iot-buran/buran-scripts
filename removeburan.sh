#!/bin/bash

docker-compose stop
docker ps -a |  grep "buran" | awk '{print $1}' | xargs docker rm
docker images -a |  grep "buran" | awk '{print $3}' | xargs docker rmi -f
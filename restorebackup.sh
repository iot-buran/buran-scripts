#!/bin/bash

db_network=$(sudo docker container inspect buran-mongo -f '{{range $index, $element := .NetworkSettings.Networks}}{{$index}}{{end}}')
echo Buran DB found on network $db_network
[ -z "$1" ] && dump_location="/home" || dump_location=$1
echo Searching for dump.gz in $dump_location
sudo docker run --rm --link buran-mongo:mongo --net=$db_network -v $dump_location:/backup mongo bash -c "mongorestore --archive=/backup/dump.gz --db buran-db --host mongo:27017"
